
import tkinter as tk

class CalculatorUi(tk.Frame):

    def __init__(self, master=None):
        super().__init__(master)
        self.calculate = Calculate()
        self.master = master
        self.pack()
        self.init_gui()

    def init_gui(self):

        self.master.title("Fraction Calculator")
        self.master.geometry("280x180")

        self.number_1 = tk.StringVar()
        self.number_2 = tk.StringVar()
        self.divisor_1 = tk.StringVar()
        self.divisor_2 = tk.StringVar()
        self.result = tk.StringVar()

        self.number_1_field = tk.Entry(self, textvariable=self.number_1, width=10)
        self.number_2_field = tk.Entry(self, textvariable=self.number_2, width=10)
        self.divisor_1_field = tk.Entry(self, textvariable=self.divisor_1, width=10)
        self.divisor_2_field = tk.Entry(self, textvariable=self.divisor_2, width=10)
        self.result_field = tk.Entry(self, textvariable=self.result, state='disabled', width=20)

        number_1_label = tk.Label(self, text='Number 1:', width=8)
        number_2_label = tk.Label(self, text='Number 2:', width=8)
        divisor_1_label = tk.Label(self, text='Divisor 1:', width=8)
        divisor_2_label = tk.Label(self, text='Divisor 2:', width=8)
        empty_label_1 = tk.Label(self, text='')
        empty_label_2 = tk.Label(self, text='')
        empty_label_3 = tk.Label(self, text='')
        result_label = tk.Label(self, text='Result:', width=8)

        self.button_add = tk.Button(self, text="Add", command=self.add)
        self.button_sub = tk.Button(self, text="Substract", command=self.sub)
        self.button_mul = tk.Button(self, text="Multiply", command=self.multiply)
        self.button_div = tk.Button(self, text="Divide", command=self.divide)
        self.button_close = tk.Button(self, text="Exit", command=self.close)

        number_1_label.grid(row=1, column=0)
        number_2_label.grid(row=1, column=2)
        divisor_1_label.grid(row=2, column=0)
        divisor_2_label.grid(row=2, column=2)
        empty_label_1.grid(row=0, column=0)
        empty_label_2.grid(row=4, column=0)
        empty_label_3.grid(row=6, column=0)
        result_label.grid(row=5, column=0)

        self.number_1_field.grid(row=1, column=1)
        self.number_2_field.grid(row=1, column=3)
        self.divisor_1_field.grid(row=2, column=1)
        self.divisor_2_field.grid(row=2, column=3)
        self.result_field.grid(row=5, column=1, columnspan=2)

        self.button_add.grid(row=3, column=0, sticky="NSEW")
        self.button_sub.grid(row=3, column=1, sticky="NSEW")
        self.button_mul.grid(row=3, column=2, sticky="NSEW")
        self.button_div.grid(row=3, column=3, sticky="NSEW")
        self.button_close.grid(row=7, column=3, sticky="NSEW")

    def add(self):

        n1, d1, n2, d2 = self.input_fields_to_int(self.number_1.get(), self.divisor_1.get(), self.number_2.get(), self.divisor_2.get())
        number, divisor = self.calculate.add(n1, d1, n2, d2)
        result = self.calculate.reduce(number, divisor)

        self.show_solution(result)

    def sub(self):

        n1, d1, n2, d2 = self.input_fields_to_int(self.number_1.get(), self.divisor_1.get(), self.number_2.get(), self.divisor_2.get())
        number, divisor = self.calculate.sub(n1, d1, n2, d2)
        result = self.calculate.reduce(number, divisor)

        self.show_solution(result)

    def multiply(self):

        n1, d1, n2, d2 = self.input_fields_to_int(self.number_1.get(), self.divisor_1.get(), self.number_2.get(), self.divisor_2.get())
        number, divisor = self.calculate.multiply(n1, d1, n2, d2)
        result = self.calculate.reduce(number, divisor)

        self.show_solution(result)

    def divide(self):

        n1, d1, n2, d2 = self.input_fields_to_int(self.number_1.get(), self.divisor_1.get(), self.number_2.get(), self.divisor_2.get())
        number, divisor = self.calculate.divide(n1, d1, n2, d2)
        result = self.calculate.reduce(number, divisor)

        self.show_solution(result)

    def input_fields_to_int(self, n1, d1, n2, d2):

        return (int(n1), int(d1), int(n2), int(d2))

    def show_solution(self, result):
    
        self.result.set("{number}/{divisor}".format(number=int(result[0]), divisor=int(result[1])))
        

    def close(self):
        
        self.destroy()
        self.master.destroy()


class Calculate():

    def multiply(self, number_1, divisor_1, number_2, divisor_2):
    
        return (number_1 * number_2, divisor_1 * divisor_2)

    def divide(self, number_1, divisor_1, number_2, divisor_2):
    
        return (number_1 * divisor_2, divisor_1 * number_2)

    def add(self, number_1, divisor_1, number_2, divisor_2):
    
        if divisor_1 == divisor_2:
            number = number_1 + number_2
            divisor = divisor_1
        else:
            number = (number_1 * divisor_2) + (number_2 * divisor_1)
            divisor = divisor_1 * divisor_2
    
        return (number, divisor)

    def sub(self, number_1, divisor_1, number_2, divisor_2):
        
        if divisor_1 == divisor_2:
            number = number_1 - number_2 
            divisor = divisor_1
        else:
            number = (number_1 * divisor_2) - (number_2 * divisor_1)
            divisor = divisor_1 * divisor_2

        return (number, divisor)
            

    def reduce(self, number, divisor):
        
        gcd = self.greatest_common_divisor(number, divisor)

        return (number / gcd, divisor / gcd)


    def greatest_common_divisor(self, n, m):

        while m != 0:
            (n, m) = (m, n % m)

        return n
    

def main():
    
    root = tk.Tk()
    app = CalculatorUi(master=root)
    app.mainloop()
    

if __name__ == '__main__':
    main()
