
import pytest
from main import Calculate


@pytest.fixture
def calculate():
    return Calculate()


# Unit-Tests
class TestCalculation():

    def test_add(self, calculate):
        assert (4, 2) == calculate.add(1, 2, 3, 2)

    def test_sub(self, calculate):
        assert (4, 2) == calculate.sub(5, 2, 1, 2)

    def test_multiply(self, calculate):
        assert (4, 2) == calculate.multiply(2, 1, 2, 2)

    def test_divide(self, calculate):
        assert (4, 2) == calculate.divide(2, 2, 1, 2)

    def test_gcd(self, calculate):
        assert 5 == calculate.greatest_common_divisor(15, 10)
