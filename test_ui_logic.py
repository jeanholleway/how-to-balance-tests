
import pytest
import tkinter as tk
from main import CalculatorUi


@pytest.fixture
def calculator_ui():
    return CalculatorUi(tk.Tk())


# Integration-Tests
class TestCalculation():

    def test_add(self, calculator_ui, mocker):
        mocker.patch('main.calculator_ui.input_fields_to_int', return_value=(2,2,2,2))
        calculator_ui.add()
        assert "2/1" == calculator_ui.result.get()

    def test_sub(self, calculator_ui, mocker):
        mocker.patch('main.calculator_ui.input_fields_to_int', return_value=(2,2,2,2))
        calculator_ui.sub()
        assert "0/1" == calculator_ui.result.get()

    def test_multiply(self, calculator_ui, mocker):
        mocker.patch('main.calculator_ui.input_fields_to_int', return_value=(2,2,2,2))
        calculator_ui.multiply()
        assert "1/1" == calculator_ui.result.get()

    def test_divide(self, calculator_ui, mocker):
        mocker.patch('main.calculator_ui.input_fields_to_int', return_value=(2,2,2,2))
        calculator_ui.divide()
        assert "1/1" == calculator_ui.result.get()
