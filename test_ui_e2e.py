
import pytest
import tkinter as tk
from main import CalculatorUi


@pytest.fixture
def calculator_ui(request):

    calc = CalculatorUi(tk.Tk())

    def teardown():
        calc.master.destroy()

    request.addfinalizer(teardown)

    return calc


# E2E-Tests
class TestCalculation():

    def test_add(self, calculator_ui):

        # set UI-input
        calculator_ui.number_1_field.delete(0, tk.END)
        calculator_ui.number_1_field.insert(0, "12")

        calculator_ui.number_2_field.delete(0, tk.END)
        calculator_ui.number_2_field.insert(0, "3")

        calculator_ui.divisor_1_field.delete(0, tk.END)
        calculator_ui.divisor_1_field.insert(0, "3")

        calculator_ui.divisor_2_field.delete(0, tk.END)
        calculator_ui.divisor_2_field.insert(0, "6")

        # calculate the result
        calculator_ui.button_add.invoke()

        # check if result in UI is correct
        assert "9/2" == calculator_ui.result.get()

    def test_sub(self, calculator_ui):

        # set UI-input
        calculator_ui.number_1_field.delete(0, tk.END)
        calculator_ui.number_1_field.insert(0, "12")

        calculator_ui.number_2_field.delete(0, tk.END)
        calculator_ui.number_2_field.insert(0, "3")

        calculator_ui.divisor_1_field.delete(0, tk.END)
        calculator_ui.divisor_1_field.insert(0, "3")

        calculator_ui.divisor_2_field.delete(0, tk.END)
        calculator_ui.divisor_2_field.insert(0, "6")

        # calculate the result
        calculator_ui.button_sub.invoke()

        # check if result in UI is correct
        assert "7/2" == calculator_ui.result.get()

    def test_multiply(self, calculator_ui):

        # set UI-input
        calculator_ui.number_1_field.delete(0, tk.END)
        calculator_ui.number_1_field.insert(0, "12")

        calculator_ui.number_2_field.delete(0, tk.END)
        calculator_ui.number_2_field.insert(0, "3")

        calculator_ui.divisor_1_field.delete(0, tk.END)
        calculator_ui.divisor_1_field.insert(0, "3")

        calculator_ui.divisor_2_field.delete(0, tk.END)
        calculator_ui.divisor_2_field.insert(0, "6")

        # calculate the result
        calculator_ui.button_mul.invoke()

        # check if result in UI is correct
        assert "2/1" == calculator_ui.result.get()

    def test_divide(self, calculator_ui):

        # set UI-input
        calculator_ui.number_1_field.delete(0, tk.END)
        calculator_ui.number_1_field.insert(0, "12")

        calculator_ui.number_2_field.delete(0, tk.END)
        calculator_ui.number_2_field.insert(0, "3")

        calculator_ui.divisor_1_field.delete(0, tk.END)
        calculator_ui.divisor_1_field.insert(0, "3")

        calculator_ui.divisor_2_field.delete(0, tk.END)
        calculator_ui.divisor_2_field.insert(0, "6")

        # calculate the result
        calculator_ui.button_div.invoke()

        # check if result in UI is correct
        assert "8/1" == calculator_ui.result.get()
